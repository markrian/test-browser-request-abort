const log = (...args) => console.log(...args);
const marks = (...args) => {
  const entries = performance
    .getEntriesByType("mark")
    .sort((a, b) => a.startTime - b.startTime)
    .map((entry) => ({ name: entry.name, startTime: entry.startTime }));

  performance.measure(
    "from initiate reload to request cancelled",
    "initiate reload",
    "request cancelled"
  );

  entries.push(
    ...performance
      .getEntriesByType("measure")
      .map((m) => ({ name: m.name, duration: m.duration }))
  );

  return JSON.stringify(entries, null, 2);
};

function reload() {
  const delay = document.querySelector('input[type="number"]').value;
  performance.mark("initiate reload");
  window.location.href = `/?delay=${delay}`;
}

async function main() {
  const delay = 2000;
  const url = "/hang";
  log(`requesting ${url}`);
  let res;

  try {
    res = await fetch(url);
  } catch (error) {
    performance.mark("request cancelled");
    log(error);
    log(marks());
  }
}

document.querySelector("button").addEventListener("click", () => {
  reload();
});

window.addEventListener("beforeunload", (event) => {
  performance.mark("beforeunload");
});

main().catch((error) => {
  console.error("UNCAUGHT ERROR!", error);
});
