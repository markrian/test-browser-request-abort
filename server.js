const express = require('express');
const app = express();
const port = 8000;

// Delay requests with delay query
app.use((req, res, next) => {
  const delay = Number(req.query.delay);

  if (delay) {
    setTimeout(next, delay);
  } else {
    next();
  }
});

app.use('/', express.static('public'));

// Requests to here always hang
app.get('/hang', (req, res) => {
  console.log(`request to ${req.url}`);
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
