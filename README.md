# Request cancelling on navigation

This attempts to demonstrate the different ways Firefox and Chrome handle
in-flight requests when navigating away from the current page.

To run:

```
npm i
npm start
```

Then open http://localhost:8000, and follow the instructions.
